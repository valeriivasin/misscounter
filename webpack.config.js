/*eslint-env node*/
module.exports = {
  entry: {
    app: ['./src/js/app.jsx']
  },
  output: {
    publicPath: 'public/js',
    path: require('path').resolve('./public/js/'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' },
      { test: /\.json?$/, loader: 'json-loader' }
    ]
  },
  plugins: []
};
