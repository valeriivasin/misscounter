var path = require('path');
var argv = require('minimist')(process.argv.slice(2));
var del = require('del');

var gulp = require('gulp');
var gutil = require('gulp-util');
var gulpif = require('gulp-if');
var less = require('gulp-less');
var preprocess = require('gulp-preprocess');
var htmlmin = require('gulp-htmlmin');
var minifyCss = require('gulp-minify-css');

var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var webpackConfig = require('./webpack.config.js');

var isProduction = argv.production;

// The development server (the recommended option for development)
// Note: you should open http://localhost:8080/public/
gulp.task('default', ['watch']);

gulp.task('watch', ['watch:files', 'webpack-dev-server']);

gulp.task('watch:files', ['html', 'img', 'less'], function() {
  gulp.watch('src/index.html', ['html']);
  gulp.watch('src/css/**/*', ['less']);
});

// Production build
gulp.task('build', [
  'webpack:build',
  'html',
  'img',
  'less'
]);

gulp.task('clean:js', function(callback) {
  del(['public/js'], callback);
});

gulp.task('webpack:build', ['clean:js'], function(callback) {
  if (isProduction) {
    webpackBuildProd(callback);
  } else {
    webpackBuildDev(callback);
  }
});

function webpackBuildProd(callback) {
  // modify some webpack config options
  var myConfig = Object.create(webpackConfig);
  myConfig.plugins = myConfig.plugins.concat(
    new webpack.DefinePlugin({
      'process.env': {
        // This has effect on the react lib size
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin()
  );

  // run webpack
  webpack(myConfig, function(err, stats) {
    if(err) throw new gutil.PluginError('webpack:build-prod', err);
    gutil.log('[webpack:build-prod]', stats.toString({
      colors: true
    }));
    callback();
  });
}

// modify some webpack config options
var myDevConfig = Object.create(webpackConfig);
myDevConfig.devtool = 'sourcemap';
myDevConfig.debug = true;

// create a single instance of the compiler to allow caching
var devCompiler = webpack(myDevConfig);

function webpackBuildDev(callback) {
  if (isProduction) {
    callback();
    return;
  }

  // run webpack
  devCompiler.run(function(err, stats) {
    if(err) throw new gutil.PluginError('webpack:build-dev', err);
    gutil.log('[webpack:build-dev]', stats.toString({
      colors: true
    }));
    callback();
  });
}

gulp.task('webpack-dev-server', function(callback) {
  // modify some webpack config options
  var myConfig = Object.create(webpackConfig);
  myConfig.devtool = 'eval';
  myConfig.debug = true;

  // Start a webpack-dev-server
  new WebpackDevServer(webpack(myConfig), {
    publicPath: '/' + myConfig.output.publicPath,
    stats: {
      colors: true
    }
  }).listen(8080, 'localhost', function(err) {
    if(err) throw new gutil.PluginError('webpack-dev-server', err);
    gutil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');
  });
});

gulp.task('clean:html', function(callback) {
  del('public/index.html', callback);
});

gulp.task('html', ['clean:html'], function() {
  return gulp
    .src('src/index.html')
    .pipe(preprocess({
      context: {
        NODE_ENV: isProduction ? 'production' : 'development'
      }
    }))
    .pipe(gulpif(
      isProduction,
      htmlmin({
        collapseWhitespace: true,
        removeComments: true
      })
    ))
    .pipe(gulp.dest('public/'));
});

gulp.task('clean:img', function(callback) {
  del('public/img', callback);
})

gulp.task('img', ['clean:img'], function() {
  return gulp
    .src('src/img/**/*')
    .pipe(gulp.dest('public/img/'));
});

gulp.task('clean:css', function(callback) {
  del('public/css', callback);
});

gulp.task('less', ['clean:css'], function() {
  return gulp.src('./src/css/app.less')
    .pipe(less({
      paths: ['./src/css']
    }).on('error', gutil.log))
    .pipe(gulpif(isProduction, minifyCss()))
    .pipe(gulp.dest('./public/css'));
});
