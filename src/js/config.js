import moment from 'moment-timezone';

/**
 * Notice:
 * Period is [separation, meetup],
 * where separation date is before meetup date
 */
let configs = [
  {
    separation: moment.tz('17.01.2016 14:00', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('30.01.2016 23:00', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('04.01.2016 14:00', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('15.01.2016 01:00', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('13.12.2015 21:00', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('23.12.2015 09:00', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('29.11.2015 20:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('12.12.2015 01:40', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('09.11.2015 18:00', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('27.11.2015 01:45', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('12.10.2015 09:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('07.11.2015 11:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('20.09.2015 09:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('08.10.2015 20:05', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('09.08.2015 22:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('03.09.2015 17:35', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('19.07.2015 09:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('05.08.2015 23:35', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('28.06.2015 21:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('16.07.2015 23:35', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('07.06.2015 11:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('27.06.2015 01:35', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('17.05.2015 11:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('03.06.2015 23:35', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  },
  {
    separation: moment.tz('03.05.2015 11:30', 'DD.MM.YYYY HH:mm', 'Europe/Berlin'),
    meetup: moment.tz('14.05.2015 23:35', 'DD.MM.YYYY HH:mm', 'Europe/Berlin')
  }
];

/**
 * Get first config with `separation` and `meetup` props
 */
let findConfig = (configs) => {
  let config = configs[0];

  if (config.separation && config.meetup) {
    return config;
  }

  return configs[1];
};

// export first item - it's latest date
export default findConfig(configs);
