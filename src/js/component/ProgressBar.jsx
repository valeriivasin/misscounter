/**
 * <ProgressBar progress={number} />
 */
import React from 'react';

let ProgressBar = React.createClass({
  render() {
    return (
      <div className="progressBar">
        <div className="progressBar__text">{this.props.progress + '%'}</div>
        <div className="progressBar__bar" style={{width: this.props.progress + '%'}}></div>
      </div>
    );
  }
});

export default ProgressBar;
