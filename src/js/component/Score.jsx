/**
 * <Score value={number} label={textLabel} />
 *
 * If provided number less then 10 - add extra zero to the beginning
 */
import React from 'react';
import Digit from './Digit.jsx';

let Score = React.createClass({
  render() {
    let numbers = this.props.value
      .toString()
      .split('')
      .map(Number);

    if (numbers.length === 1) {
      numbers.unshift(0);
    }

    let digits = numbers.map((stringifiedNum, index) => {
      return (
        <div key={index} className="score__wrap__digit">
          <Digit value={Number(stringifiedNum)} />
        </div>
      );
    });

    return (
      <div className="score">
        <div className="score__digits">{digits}</div>
        <div className="score__label">{this.props.label}</div>
      </div>
    );
  }
});

export default Score;
