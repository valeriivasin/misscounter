/**
 * <Digit value={number} />
 */

import React from 'react';

let Digit = React.createClass({
  render() {
    return (
      <div className="digit">
        {this.props.value}
      </div>
    );
  }
});

export default Digit;
