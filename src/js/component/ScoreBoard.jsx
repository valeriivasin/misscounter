/**
 * <ScoreBoard scores={[
 *   { value: 25, label: 'days' },
 *   { value: 13, label: 'hours' },
 * ]} />
 */
import React from 'react';
import Score from './Score.jsx';

let ScoreBoard = React.createClass({
  render() {
    let scores = this.props.scores.map((score) => {
      return (
        <div key={score.label + score.value} className="scoreBoard__wrap__score">
          <Score value={score.value} label={score.label} />
        </div>
      );
    });

    return (
      <div className="scoreBoard">{scores}</div>
    );
  }
});

export default ScoreBoard;
