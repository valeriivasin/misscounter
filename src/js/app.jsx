import React from 'react';
import moment from 'moment-timezone';

import ScoreBoard from './component/ScoreBoard.jsx';
import ProgressBar from './component/ProgressBar.jsx';
import { separation, meetup } from './config';

function getAppState() {
  let now = moment();

  if (now > meetup) {
    return { love: true };
  }

  let stats = getStats();
  let scores = [
      { value: stats.days, label: 'days' },
      { value: stats.hours, label: 'hours' },
      { value: stats.minutes, label: 'minutes' },
      { value: stats.seconds, label: 'seconds' }
  ];

  return {
    love: false,
    progress: getProgress(),
    scores: scores
  };
}

function getProgress() {
  let now = moment();
  let secondsTotal = meetup.diff(separation, 'seconds');
  let secondsLeft = now.diff(separation, 'seconds');

  return Math.round(
    (secondsLeft / secondsTotal) * 100
  );
}

function getStats() {
  let now = moment();
  let seconds = meetup.diff(now, 'seconds');
  let minutes = meetup.diff(now, 'minutes');
  let hours   = meetup.diff(now, 'hours');
  let days    = meetup.diff(now, 'days');

  return {
    days: days,
    hours: hours % 24,
    minutes: minutes % 60,
    seconds: seconds % 60
  };
}

let App = React.createClass({
  componentDidMount() {
    this.tick();
  },

  tick: function() {
    let state = getAppState();

    if (!state.love) {
      setTimeout(() => {
        this.tick();
      }, 1000);
    }

    this.setState(state);
  },

  getInitialState() {
    return getAppState();
  },

  render() {
    if (this.state.love) {
      return (
        <div className="app app--love" />
      );
    }

    return (
      <div className="app">
        <div className="app__wrap__scoreBoard">
          <ScoreBoard scores={this.state.scores} />
        </div>

        <div className="app__wrap__progressBar">
          <ProgressBar progress={this.state.progress} />
        </div>
      </div>
    );
  }
});

React.render(<App />, document.body);
